package com.classpath.kstreamsstate.transformer;

import com.classpath.kstreamsstate.model.Purchase;
import com.classpath.kstreamsstate.model.RewardAccumulator;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;
import java.util.Objects;

public class PurchaseRewardTransformer implements ValueTransformer<Purchase, RewardAccumulator> {

    private KeyValueStore<String, Integer> stateStore;
    private final String storeName;
    private ProcessorContext context;

    public PurchaseRewardTransformer(String storeName) {
        Objects.requireNonNull(storeName,"Store Name can't be null");
        this.storeName = storeName;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context;
        stateStore = (KeyValueStore) this.context.getStateStore(storeName);
    }

    @Override
    public RewardAccumulator transform(Purchase purchase) {
        RewardAccumulator rewardAccumulator = RewardAccumulator.builder()
                                                    .customerId(purchase.getLastName()+","+purchase.getFirstName())
                                                    .purchaseTotal( purchase.getPrice() * (double) purchase.getQuantity())
                                                    .currentRewardPoints((int) (purchase.getPrice() * (double) purchase.getQuantity()))
                                                    .totalRewardPoints((int)(purchase.getPrice() * (double) purchase.getQuantity()))
                                                    .build();
        Integer accumulatedSoFar = stateStore.get(rewardAccumulator.getCustomerId());

        if (accumulatedSoFar != null) {
            rewardAccumulator.addRewardPoints(accumulatedSoFar);
        }
        stateStore.put(rewardAccumulator.getCustomerId(), rewardAccumulator.getTotalRewardPoints());

        return rewardAccumulator;

    }

    @Override
    public void close() {
        //nothing to perform..
    }
}
