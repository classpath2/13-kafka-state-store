package com.classpath.kstreamsstate.model;

import lombok.*;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Builder
public class RewardAccumulator {
    private String customerId;
    private double purchaseTotal;
    private int totalRewardPoints;
    private int currentRewardPoints;
    private int daysFromLastPurchase;

    public void addRewardPoints(int previousTotalPoints) {
        this.totalRewardPoints += previousTotalPoints;
    }
}
