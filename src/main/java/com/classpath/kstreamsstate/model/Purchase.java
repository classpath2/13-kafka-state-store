package com.classpath.kstreamsstate.model;


import lombok.*;

import java.util.Date;
import java.util.Objects;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Builder
public class Purchase {

    private String firstName;
    private String lastName;
    private String customerId;
    private String creditCardNumber;
    private String itemPurchased;
    private String department;
    private String employeeId;
    private int quantity;
    private double price;
    private Date purchaseDate;
    private String zipCode;
    private String storeId;
    private static final String CC_NUMBER_REPLACEMENT="xxxx-xxxx-xxxx-";


        public Purchase maskCreditCard(){
            Objects.requireNonNull(this.creditCardNumber, "Credit Card can't be null");
            String[] parts = this.creditCardNumber.split("-");
            if (parts.length < 4 ) {
                this.creditCardNumber = "xxxx";
            } else {
                String last4Digits = this.creditCardNumber.split("-")[3];
                this.creditCardNumber = CC_NUMBER_REPLACEMENT + last4Digits;
            }
            return this;
        }

}
