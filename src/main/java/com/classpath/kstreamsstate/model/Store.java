package com.classpath.kstreamsstate.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Store {
    private String employeeId;
    private String zipCode;
    private String storeId;
    private String department;

}