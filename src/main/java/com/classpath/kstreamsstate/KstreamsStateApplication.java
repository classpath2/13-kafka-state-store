package com.classpath.kstreamsstate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KstreamsStateApplication {

    public static void main(String[] args) {
        SpringApplication.run(KstreamsStateApplication.class, args);
    }

}
